
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--HavreCube by Lipki                                                                  +
--                                                                                    +
--LICENSE: WTFPL DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE; applies to all parts.   +
--                                                                                    +
--Please give any ideas on ways to improve this mod!                                  +
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- define stuff
havrecube = {
    origin={x=0, y=1000, z=0},
    spawnzone={x=10, z=10},
    spawnsize={x=10, z=10},
    cubestart="havrecube:havrecube",
    cubeback="havrecube:havrecubeback",
}

--
-- Cube
--

minetest.register_node( havrecube.cubestart, {
	description = "Havre Cube",
	groups = {dig_immediate=2},
	sunlight_propagates = true,
	paramtype = "light",
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
    },
	tile_images = {
        "havrecube_top.png",
        "havrecube_bottom.png",
        "havrecube_right.png",
        "havrecube_left.png",
        "havrecube_back.png",
        "havrecube_front.png",
    },
	sounds = default.node_sound_stone_defaults(),
	on_rightclick = function(pos, node, clicker, itemstack)
            
        print("Havre - Search")
        
        local ppos = clicker:getpos()
        local metago = minetest.get_meta(pos)
        local hp
        
        if metago:get_string("owner") == '' then
            
            local ho = havrecube.origin
            local cible, meta
            local h  = false
            local nh = false
            local hs = false
            
            print("Havre - init")
            for lx = 0, havrecube.spawnzone.x, 1 do
                for lz = 0, havrecube.spawnzone.z, 1 do
                    hp = {x=lx*havrecube.spawnsize.x+ho.x, y=ho.y, z=lz*havrecube.spawnsize.z+ho.x}
                    cible = minetest.get_node(hp)
                    --print(hp.x, hp.y, hp.z, cible.name)
                    if cible.name == "air" then
                        nh = hp
                        hs = true
                    else
                        meta = minetest.get_meta(hp)
                        print( '--', meta:get_string("owner") )
                        if meta:get_string("owner") == clicker:get_player_name() then
                            h = hp
                            hs = true
                        end
                    end
                    if hs then break end
                end
                if hs then break end
            end
            
            print("Havre - Create")
            if nh then
                minetest.env:add_node(hp, {name=havrecube.cubeback})
                for ix = -2, 2, 1 do
                    for iz = -2, 2, 1 do
                        if ix ~= 0 or iz ~= 0 then
                            minetest.env:remove_node( {x=hp.x+ix, y=hp.y+1, z=hp.z+iz}, {name="default:air"})
                            minetest.env:remove_node( {x=hp.x+ix, y=hp.y  , z=hp.z+iz}, {name="default:air"})
                            if ix == -2 or iz == -2 or ix == 2 or iz == 2 then
                                minetest.env:add_node({x=hp.x+ix, y=hp.y+1, z=hp.z+iz}, {name="default:fence_wood"})
                                minetest.env:add_node({x=hp.x+ix, y=hp.y  , z=hp.z+iz}, {name="default:dirt_with_grass"})
                            else
                                minetest.env:add_node({x=hp.x+ix, y=hp.y  , z=hp.z+iz}, {name="default:dirt_with_grass"})
                            end
                        end
                    end
                end
            
                metago:set_string("owner", clicker:get_player_name())
                metago:set_string("x", hp.x)
                metago:set_string("y", hp.y)
                metago:set_string("z", hp.z)
            end
        
        else
        
            hp = {x=metago:get_string("x"), y=metago:get_string("y"), z=metago:get_string("z")}
            
        end
        
        print("Havre - Player TP go - ", hp.x, hp.y, hp.z)
        clicker:setpos({x=hp.x, y=hp.y+3, z=hp.z});
            
        print("Havre - associate")
        local metaback = minetest.get_meta(hp)
        metaback:set_string("owner", clicker:get_player_name())
        metaback:set_string("x", ppos.x)
        metaback:set_string("y", ppos.y)
        metaback:set_string("z", ppos.z)
        
        print('--------------')
        
	end,

	after_place_node = function(pos, placer)
		print('after_place_node')
	end,

	on_use = function(itemstack, user, pointed_thing)
		print('on_use')
	end,

	on_timer = function(pos)
		print('on_timer')
	end,
})

minetest.register_node( havrecube.cubeback, {
	description = "Havre Cube Back",
	sunlight_propagates = true,
	paramtype = "light",
	light_source = LIGHT_MAX,
	drawtype = "nodebox",
	node_box = {
		type="fixed",
		fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
	},
	tile_images = { "havrecube_out.png" },
	sounds = default.node_sound_leaves_defaults(),
	on_rightclick = function(pos, node, clicker, itemstack)
    
        print("Havre - locate")
        
        local metaback = minetest.get_meta(pos)
        hp = {x=metaback:get_string("x"), y=metaback:get_string("y"), z=metaback:get_string("z")}
        
        print("Havre - Player TP back - ", hp.x, hp.y, hp.z)
        clicker:setpos({x=hp.x, y=hp.y, z=hp.z});
        
        print('--------------')
        
	end
})

--
-- Crafting
--

minetest.register_craft({
	output = havrecube.cubestart,
	recipe = {
		{'default:dirt', 'default:dirt', 'default:dirt'},
		{'default:dirt', 'default:dirt', 'default:dirt'},
		{'default:dirt', 'default:dirt', 'default:dirt'},
	}
})

--
-- Mod Havre Cube Loaded !
--

print("Mod Havre Cube Loaded !")
